<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet"> 
	<link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
	<title>Document</title>
</head>
<body>
	<div class="dropdown-menu">
		<a href="#" class="header__link nav__link">Новости</a>
		<a href="#" class="header__link nav__link">FAQ</a>
		<a href="#" class="header__link nav__link">Отзывы</a>
		<a href="#" class="header__link nav__link">Обратная связь</a>
		<a href="" class="header__link header__sign-in"><i class="fa fa-sign-in" aria-hidden="true"></i>Вход</a>
		<a href="" class="header__link header__sign-up"><i class="fa fa-user-plus" aria-hidden="true"></i>Регистрация</a>
	</div>
	<section class="about-game">
			<div class="row">
			<header class="header">
				<div class="header__item header__nav">
					<a href="#" class="header__link header__logo-wrapper">
						<img src="img/logo.png" class="header__logo" alt="">
					</a>
					<a href="#" class="header__link nav__link">Новости</a>
					<a href="#" class="header__link nav__link">FAQ</a>
					<a href="#" class="header__link nav__link">Отзывы</a>
					<a href="#" class="header__link nav__link">Обратная связь</a>
				</div>
				<div class="header__item header__authorization">
					<a href="" class="header__link header__sign-in"><i class="fa fa-sign-in" aria-hidden="true"></i>
	Вход</a>
					<a href="" class="header__link header__sign-up"><i class="fa fa-user-plus" aria-hidden="true"></i>Регистрация</a>
				</div>
				<button class="header__menu-btn">
					<div class="menu-btn__line"></div>
					<div class="menu-btn__line"></div>
					<div class="menu-btn__line"></div>
				</button>
		</header>
			<h1 class="about-game__heading section__heading">Играй и зарабатывай</h1>
			<p class="about-game__text">Наша игра - инновационный и увлекательный проект, в котором любой пользователь может попробовать себя в роли предпринимателя</p>
			<a href="#" class="about-game__btn section__btn"><i class="fa fa-flag-checkered" aria-hidden="true"></i>
Поехали!</a>
		</div>
	</section>
	<section class="about-game__description">
		<div class="row">
			<div class="description__text-wrapper">
				<h2 class="description__heading section__heading">Доставка грузов за 24 часа</h2>
				<div class="description__text">
					<p>Зарабатывать реальные деньги никогда еще не было так просто</p>
					<p>В течение 24 часов вы должны доставить груз на арендованной машине из пункта <span>a</span> в пункт <span>b</span>, преодолев на своем пути 3 заправки</p>
					<p>И в конце этого приключения получайте реальные деньги на свой счёт. Окунитесь в мир логистики и маркетинга вместе с нами</p>
				</div>
			</div>
			<div class="description__map-wrapper">
				<img src="img/description-map.png" alt="" class="description__map">
			</div>
		</div>
	</section>
	<section class="how-to-play">
		<div class="row">
			<h2 class="how-to-play__heading section__heading">Как играть</h2>
			<div class="how-to-play__wrapper">
				<div class="wrapper__item">
					<div class="img__wrapper first">
						<div class="item__img"></div>
						<div class="item__text">Арендуете авто</div>
					</div>
					<div class="item__description">
						Выбирайте и оплачивайте аренду машины
					</div>
				</div>
				<div class="wrapper__item">
					<div class="img__wrapper second">
						<div class="item__img"></div>
						<div class="item__text">Приглашаете друзей</div>
					</div>
					<div class="item__description">
						За 24 часа приглашайте друзей, чтобы они оплатили вам заправки или случайные пользовтели могут вам их оплатить
					</div>
				</div>
				<div class="wrapper__item">
					<div class="img__wrapper third">
						<div class="item__img"></div>
						<div class="item__text">Получайте прибыль</div>
					</div>
					<div class="item__description">
						Получайте прибыль на свой счет. Выводите деньги любым удобным способом
					</div>
				</div>
<!--
				<div class="wrapper__item"></div>
				<div class="wrapper__item"></div>
-->
			</div>
			<div class="btn__wrapper">
				<a href="#" class="how-to-play__btn section__btn">Начать игру</a>
			</div>
		</div>
	</section>
	<section class="advantages">
		<div class="row">
			<h2 class="section__heading advantages__heading">Наши преимущества</h2>
			<div class="advantages__text">
				Вы получаете прибыль вне зависимости от того успели ли вы добраться до конца или нет
			</div>
			<div class="advantages__wrapper">
				<div class="wrapper__item">
					<div class="image__wrapper">
						<img src="img/cars.png" alt="" class="item__image">
					</div>
					<div class="item__heading">4 вида машин</div>
					<div class="item__text">
						Газель за 100р
						Volkswagen за 250р
						Peogeot за 500р
						Mercedes за 1000р
					</div>
				</div>
				<div class="wrapper__item">
					<div class="image__wrapper">
						<img src="img/FREE.png" alt="" class="item__image">
					</div>
					<div class="item__heading">Платите за друга</div>
					<div class="item__text">
						Оплачивая заправку для друга - вам совершенно бесплатно даётся собственная машина в аренду
					</div>
				</div>
				<div class="wrapper__item">
					<div class="image__wrapper">
						<img src="img/friends.png" alt="" class="item__image">
					</div>
					<div class="item__heading">Ищите друзей</div>
					<div class="item__text">
						Заправку вам могут оплатить случайные пользователи нашей системы
					</div>
				</div>
				<div class="wrapper__item">
					<div class="image__wrapper">
						<img src="img/gifts.png" alt="" class="item__image">
					</div>
					<div class="item__heading">Конкурсы</div>
					<div class="item__text">
						Побеждай в еженедельных конкурсах за главный приз <span>iPhone X</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="subscribe">
		<div class="row">
			<h2 class="subscribe__heading section__heading">Подписаться на рассылку</h2>
			<div class="subscribe__text">Подпишитесь на рассылку, чтобы быть в курсе наших новостей</div>
			<form action="" class="subscribe__form">
				<input type="text" placeholder="Введите свой E-mail адрес" class="subscribe__input subscribe__e-mail">
				<div class="subscribe__input-wrapper">
					<i class="fa fa-paper-plane fa-2x" aria-hidden="true"></i>
					<input type="submit" value='' class="subscribe__input subscribe__submit">
				</div>
			</form>
		</div>
	</section>
	<section class="competitions">
		<div class="row">
			<h2 class="section__heading">
				Конкурсы
			</h2>
			<div class="competitions__date">
				Еженедельный конкурс 14.01 &mdash; 21.01
			</div>
			<div class="prizes-wrapper">
				<div class="prize-item">
						<img src="img/competitions-prize2.png" alt="" class="item__image">
					<div class="item__description">
						1-ое место
						<div class="item__name">
							Приз: Iphone X
						</div>
					</div>
				</div>
				<div class="prize-item">
					<div class="item__description">
						2-ое место
						<div class="item__name">
							Приз: Monster Beats
						</div>
					</div>
						<img src="img/competitions-prize1.png" alt="" class="item__image">
				</div>
				<div class="prize-item">
						<img src="img/competitions-prize3.png" alt="" class="item__image">
					<div class="item__description">
						3-ое место
						<div class="item__name">
							Приз: USB 2 Tb
						</div>
					</div>
				</div>
			</div>
			<div class="statistics__btn-wrapper">
				<a href="#" class="section__btn">
					Начать игру
				</a>
			</div>
		</div>
	</section>
	<section class="feedback">
		<div class="row">
			<h2 class="feedback__heading section__heading">
				Отзывы
			</h2>
			<div class="feedback__wrapper">
				<div class="feedback__item">
					<div class="item__profile">
						<div class="image__wrapper">
							<img src="img/profile1-photo.jpg" alt="" class="item__picture">
						</div>
						<div class="item__info">
							<div class="profile__name">
								Антон Самойлов
							</div>
							<div class="profile__age">
								16 лет г.Екатеринбург
							</div>
						</div>
					</div>
					<div class="item__text">
						Меня пригласил друг в ВК, попросил заправить его в игре всего за 100 рублей. Я не заметил как и сам втянулся, и вот сам уже арендовал 10 машин, 8 из которых доставили груз до конечной точки
					</div>
				</div>
				<div class="feedback__item">
					<div class="item__profile">
						<div class="image__wrapper">
							<img src="img/profile4-photo.jpg" alt="" class="item__picture">
						</div>
						<div class="item__info">
							<div class="profile__name">
								inFamous99
							</div>
							<div class="profile__age">
								20 лет г.Иркутск
							</div>
						</div>
					</div>
					<div class="item__text">
						Научился зарабатывать здесь по 1000р в день! Просто рассылаю свою ссылку в социальных сетях и прошу помочь кому по 100 руб, кому по 500. Все друзья уже здесь залипают)
					</div>
				</div>
				<div class="feedback__item">
					<div class="item__profile">
						<div class="image__wrapper">
							<img src="img/profile2-photo.jpg" alt="" class="item__picture">
						</div>
						<div class="item__info">
							<div class="profile__name">
								Галина Полянская
							</div>
							<div class="profile__age">
								18 лет г.Москва
							</div>
						</div>
					</div>
					<div class="item__text">
						Из игр я уже выросла, но меня привлекло то, что можно реально заработать денег здесь, не прилагая особых усилий. Самое главное, что почти никого не приглашала! А заправки мне все равно оплачивали случайные люди.
					</div>
				</div>
				<div class="feedback__item">
					<div class="item__profile">
						<div class="image__wrapper">
							<img src="img/profile3-photo.jpg" alt="" class="item__picture">
						</div>
						<div class="item__info">
							<div class="profile__name">
								Маша
							</div>
							<div class="profile__age">
								18 лет г.Москва
							</div>
						</div>
					</div>
					<div class="item__text">
						Из игр я уже выросла, но меня привлекло то, что можно реально заработать денег здесь, не прилагая особых усилий. Самое главное, что почти никого не приглашала! А заправки мне все равно оплачивали случайные люди. Моя рефералка на Mercedes Sprinter: https://cargo24hr.com/mashutka?=105359
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="statistics">
		<div class="row">
			<h2 class="section__heading statistics__heading">Статистика</h2>
			<div class="statistics__wrapper">
				<div class="statistics__block">
					<div class="statistics__last-competition">
						<div class="last-competition__heading">
							Последний конкурс
						</div>
						<div class="last-competition__profile">
							<div class="profile__image-wrapper">
								<div class="profile__place">1</div>
								<img src="img/competition-profile1.jpg" alt="" class="profile__image">
							</div>
							<div class="profile__info">
								Алена Ванторина
								<div class="profile__prize">Приз: Iphone 6S</div>
							</div>
						</div>
						<div class="last-competition__profile">
							<div class="profile__image-wrapper">
								<div class="profile__place">2</div>
								<img src="img/competition-profile2.jpg" alt="" class="profile__image">
							</div>
							<div class="profile__info">
								Андрей Васильев
								<div class="profile__prize">Приз: Наушники Monster Beats</div>
							</div>
						</div>
						<div class="last-competition__profile">
							<div class="profile__image-wrapper">
								<div class="profile__place">3</div>
								<img src="img/competition-profile3.jpg" alt="" class="profile__image">
							</div>
							<div class="profile__info">
								Сергей Литвинов
								<div class="profile__prize">Приз: USB 2Tb</div>
							</div>
						</div>
					</div>
					<div class="statistics__btn-wrapper">
						<a href="#" class="section__btn">
							Начать игру
						</a>
						<a href="#" class="section__btn personal-cabinet">
							Личный кабинет
						</a>
					</div>
				</div>
				<div class="statistics__block">
					<div class="statistics__detail">
						<div class="details__heading details__item">
							Рублей выведено за год
							<div class="details__image-wrapper">
								<img src="img/details-pic1.png" alt="" class="details__image">
							</div>
						</div>
						<div class="details__index details__item">
							775 556 489
						</div>
					</div>
					<div class="statistics__detail">
						<div class="details__heading details__item">
							Арендованных авто
							<div class="details__image-wrapper">
								<img src="img/details-pic2.png" alt="" class="details__image">
							</div>
						</div>
						<div class="details__index details__item">
							1 600 000
						</div>
					</div>
					<div class="statistics__detail">
						<div class="details__heading details__item">
							Пользователей в системе
							<div class="details__image-wrapper">
								<img src="img/details-pic3.png" alt="" class="details__image">
							</div>
						</div>
						<div class="details__index details__item">
							3 548 920
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer class="footer">
		<div class="row">
			<div class="footer__top">
				<div class="footer__item">
					<div class="footer__logo-wrapper">
						<img class="footer__logo" src="img/logo.png" alt="">
					</div>
					<div class="footer__game-description">
						Наша игра &mdash; это инновационный и увлекательный проект, в котором любой пользователь может попробовать себя в роли
					</div>
					<div class="footer__social-links">
						<a href="#" class="fa fa-vk"></a>
						<a href="#" class="fa fa-instagram"></a>
						<a href="#" class="fa fa-youtube"></a>
						<a href="#" class="fa fa-google-plus"></a>
						<a href="#" class="fa fa-facebook"></a>
						<a href="" class="fa fa-twitter"></a>
					</div>
				</div>
				<div class="footer__item">
					<h2 class="item__header">Навигация по сайту</h2>
					<ul class="nav__wrapper">
						<li class="nav__link"><a href="#">Новости</a></li>
						<li class="nav__link"><a href="#">FAQ</a></li>
						<li class="nav__link"><a href="#">Отзывы</a></li>
						<li class="nav__link"><a href="#">Обратная связь</a></li>
					</ul>
				</div>
			</div>
			<div class="footer__bottom">
				<div class="footer__item">
					<div class="item__image-wrapper">
						<img src="img/footer-item-pic1.png" alt="" class="item__image">
					</div>
					<div class="item__info">
						<a href="tel:8 800 500 4477">8 800 500 4477</a>
						<div class="item__description">
							С Пн. &mdash; Пт. 10<span>00</span> &mdash; 18<span>00</span>
						</div>
					</div>
				</div>
				<div class="footer__item">
					<div class="item__image-wrapper">
						<img src="img/footer-item-pic2.png" alt="" class="item__image">
					</div>
					<div class="item__info">
						<a href="mailto:info@cargo24hr.ru">info@cargo24hr.ru</a>
						<div class="item__description">
							Онлайн поддержка
						</div>
					</div>
				</div>
				<div class="footer__item">
					<div class="item__image-wrapper">
						<img src="img/footer-item-pic3.png" alt="" class="item__image">
					</div>
					<div class="item__info">
						Москва
						<div class="item__description">
							Садовническая ул., 70с2
						</div>
					</div>
				</div>
			</div>
			<div>
				&copy; 2018 &mdash; Доставка грузов за 24 часа
			</div>
		</div>
	</footer>
	<script>
		var menuBtn = document.getElementsByClassName('header__menu-btn')[0];
		var menu = document.getElementsByClassName('dropdown-menu')[0];
		var body  = document.getElementsByTagName('body')[0];
		
		menuBtn.addEventListener('click', function() {
			menuBtn.classList.toggle('menu-btn-active');
			menu.classList.toggle('dropdown-menu-active');
			body.classList.toggle('overflow-hidden');
		});
	</script>
</body>
</html>